package com.greatlearning.jdbc;

import java.sql.*;
import java.util.Scanner;

public class UserDAO {
    Scanner scanner = new Scanner(System.in);

    public void registerUser(Connection connection) throws SQLException {
        User user = new User();
        String query = "insert into users (userId, firstName, lastName, email) values (?,?,?,?)";
        System.out.printf("\n --- please enter user details to register---");
        System.out.printf("\nUser Id\n");
        int userId = Integer.parseInt(scanner.nextLine());
        user.setUserId(userId);

        System.out.printf("First_name Last_name\n");
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        user.setFirstName(firstName);
        user.setLastName(lastName);

        System.out.printf("Email Id\n");
        String email = scanner.nextLine();
        user.setEmail(email);

        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, user.getUserId());
        preparedStatement.setString(2, user.getFirstName());
        preparedStatement.setString(3, user.getLastName());
        preparedStatement.setString(4, user.getEmail());
        preparedStatement.executeUpdate();
        System.out.printf(" --- user registered successfully ---");
    }

    public void displayUsers(Statement statement) throws SQLException {
        String query = "select userId, firstName, lastName, email from users";
        ResultSet results = statement.executeQuery(query);
        System.out.printf("\nUserId " + "First_name " + "Last_name " + "Email\n");
        while (results.next()) {
            System.out.println(results.getInt("userId")
                    + " " + results.getString("firstName")
                    + " " + results.getString("lastName")
                    + " " + results.getString("email"));
        }
    }

    public void deleteUser(Connection connection) throws SQLException {
        String query = "delete from users where userId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        System.out.printf("\nEnter the user id to be deleted\n");
        int userId = scanner.nextInt();
        preparedStatement.setInt(1, userId);
        preparedStatement.executeUpdate();
        System.out.printf(" --- user " + userId + " deleted successfully ---\n");
    }

    public void updateUser(Connection connection) throws SQLException {
        System.out.printf("\nselect user id to update\n");
        int userId = Integer.parseInt(scanner.nextLine());

        System.out.printf("\nprovide value for firstname to update\n");
        String value = scanner.nextLine();
        String query = "update users set firstName = ? where userId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, value);
        preparedStatement.setInt(2, userId);
        preparedStatement.executeUpdate();
        System.out.printf(" --- user " + userId + " updated successfully ---\n");
    }
}
