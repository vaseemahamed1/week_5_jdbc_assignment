package com.greatlearning.jdbc;

import java.sql.*;
import java.util.Scanner;

public class App 
{
   
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        UserDAO userDAO = new UserDAO();

        try {
        	// username and password of mysql db is not know, using h2 for local testing
//          Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/userAccount", "root", "Admin");
            
        	Connection conn = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "", "");
            Statement statement = conn.createStatement();
            statement.execute("CREATE TABLE users(userId int primary key, firstName varchar(255),lastName varchar(255),email varchar(255))");

            int menuChoice;
            do {
                System.out.printf("\n !!!! Welcome to User CURD Services !!!!\n");
                System.out.printf("--------------------------------------------");
                System.out.printf("\n Enter the operation that you want to perform");
                System.out.printf("\n 1. Registration \n 2. Update \n 3. Display data \n 4. Delete \n 0. Exit\n");
                System.out.printf("--------------------------------------------\n");
                menuChoice = input.nextInt();

                switch (menuChoice) {
                    case 1:
                         userDAO.registerUser(conn);
                        break;
                    case 2:
                        userDAO.updateUser(conn);
                        break;
                    case 3:
                        userDAO.displayUsers(statement);
                        break;
                    case 4:
                        userDAO.deleteUser(conn);
                        break;
                }

            } while (menuChoice > 0);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
}
